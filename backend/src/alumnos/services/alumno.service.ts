import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { AlumnoDto, CreateAlumnoDto, UpdateAlumnoDto } from '../dtos/alumno.dto';
import { Alumno } from '../entities/alumno.entities';

@Injectable()
export class AlumnoService {
    constructor(
        @InjectRepository(Alumno) private alumnoRepo: Repository<Alumno>,
    ) { }

    async findAll(): Promise<AlumnoDto[]> {
        const alumnos: AlumnoDto[] = await this.alumnoRepo.find({
        });
        return alumnos.map((alumno: Alumno ) => plainToClass(AlumnoDto, alumno))
    }

    async findOne(id): Promise<AlumnoDto> {
        const alumno: Alumno = await this.alumnoRepo.findOne({
            idAlumno: id
        });
        if (!alumno) {
            throw new NotFoundException(`Alumno #${id} no encontrado`);
        }
        return plainToClass(AlumnoDto, alumno)    
    }

    async create(data: CreateAlumnoDto): Promise<AlumnoDto> {
        const nuevoAspirante = this.alumnoRepo.create(data);
        console.log(nuevoAspirante)
        const alumno = await this.alumnoRepo.save(nuevoAspirante);
        return plainToClass(AlumnoDto, alumno)    
    }

    async update(id: number, changes: UpdateAlumnoDto): Promise<AlumnoDto> {
        const alumno = await this.alumnoRepo.findOne(id);
        this.alumnoRepo.merge(alumno, changes);
        const alumnoGuardado = await this.alumnoRepo.save(alumno);
        return plainToClass(AlumnoDto, alumnoGuardado)    

    }
}
