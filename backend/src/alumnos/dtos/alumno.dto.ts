import {
    IsString,
    IsNumber,
    IsUrl,
    IsNotEmpty,
    IsPositive,
    IsEmail,
    IsDate,
    IsInt,
    IsBoolean,
    IsOptional,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class AlumnoDto {
    @Expose()
    @ApiProperty()
    readonly idAlumno: number;

    @Expose()
    @ApiProperty({ description: `Fecha de nacimiento` })
    readonly fechaNacimiento: Date;

    @Expose()
    @ApiProperty({ description: `Nombre del padre` })
    readonly nombrePadre: string;

    @Expose()
    @ApiProperty({ description: `Nombre del padre` })
    readonly nombreMadre: string;

    @Expose()
    @ApiProperty({ description: `Grado` })
    readonly grado: string;

    @Expose()
    @ApiProperty({ description: `Sección` })
    readonly seccion: string;
}

export class CreateAlumnoDto {
    // @IsDate()
    @IsNotEmpty()
    @ApiProperty({ description: `Fecha de nacimiento` })
    readonly fechaNacimiento: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: `Nombre del padre` })
    readonly nombrePadre: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: `Nombre de la madre` })
    readonly nombreMadre: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: `Grado` })
    readonly grado: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: `Sección` })
    readonly seccion: string;
}

export class UpdateAlumnoDto extends PartialType(CreateAlumnoDto) { }
