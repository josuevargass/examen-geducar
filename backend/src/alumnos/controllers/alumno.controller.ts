import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOperation, ApiProperty, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { AlumnoService } from '../services/alumno.service';
import { TransformInterceptor } from 'src/providers/transform.interceptor';
import { IOperationResult } from 'src/providers/IOperationResult';
import { AlumnoDto, CreateAlumnoDto, UpdateAlumnoDto } from '../dtos/alumno.dto';
import { AuthGuard } from '@nestjs/passport';


@ApiTags('alumnos')
@Controller('alumno')
@UseGuards(AuthGuard('api-key'))
@ApiBearerAuth()
@ApiSecurity('Authorization')
export class AlumnoController {
    constructor(private alumnoService: AlumnoService) { }
    
    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 200, description: 'Success.', type: AlumnoDto })
    @ApiResponse({ status: 500, description: 'Server error.', type: IOperationResult })
    @Get()
    @HttpCode(HttpStatus.ACCEPTED)
    getAlumnos() {
        return this.alumnoService.findAll();
    }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 200, description: 'Success.', type: AlumnoDto })
    @ApiResponse({ status: 500, description: 'Server error.', type: IOperationResult })
    @Get(':id')
    @HttpCode(HttpStatus.ACCEPTED)
    getAlumno(@Param('id') id: number) {
        return this.alumnoService.findOne(id);
    }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 500, description: 'Server error.', type: IOperationResult })
    @Put(':id')
    updateAlumno(@Param('id') id: number, @Body() payload: UpdateAlumnoDto) {
        return this.alumnoService.update(id, payload);
    }

    @UseInterceptors(TransformInterceptor)
    @ApiResponse({ status: 500, description: 'Server error.', type: IOperationResult })
    @Post()
    createProducto(@Body() payload: CreateAlumnoDto) {
        return this.alumnoService.create(payload);
    }
}
