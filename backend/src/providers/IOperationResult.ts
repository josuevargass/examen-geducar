import {
    IsString,
    IsNotEmpty,
} from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';
import { extend } from 'joi';

export class IOperationResult<T>  {
    @ApiProperty()
    result: T

    @ApiProperty()
    message: string

    @ApiProperty()
    statusCode: HttpStatus
}


