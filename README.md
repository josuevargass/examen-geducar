Las tecnologías usadas fueron NEST JS (BACK END) & IONIC ANGULAR (FRONT END).

Se implementó una arquitectura de módulos y servicios que ofrece Nest junto con protección de API por medio de Header (x_api_key), se subió a GIT el archivo .env donde están todas las credenciales del proyecto y claves básicas. Además se usó docker para la base de datos MYSQL, a través de Docker Compose.

En el front end se implementó los estilos de Ionic Framework, se realizó una lista de alumnos, y las funcionalidades de crear y editar.
