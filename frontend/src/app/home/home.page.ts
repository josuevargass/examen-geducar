import { AfterViewInit, Component } from '@angular/core';
import { Subject } from 'rxjs';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  alumnos: any[] = [];

  constructor(private _apiService: ApiService) {}

  ionViewWillEnter(){
    this._apiService.getAlumnos().subscribe((res:any)=>{
      this.alumnos = res.result;
    })
  }

  editar(){
    console.log('Editar');
  }

}
