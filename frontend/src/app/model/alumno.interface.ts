export interface Alumno {
    idAlumno?: number,
    fechaNacimiento?: string,
    nombrePadre?: string,
    nombreMadre?: string,
    grado?: string,
    seccion?: string
}