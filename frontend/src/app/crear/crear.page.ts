import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.page.html',
  styleUrls: ['./crear.page.scss'],
})
export class CrearPage implements OnInit {

  formGroup: FormGroup;

  constructor(
    private router: Router,
    private loadingController: LoadingController,
    public toastController: ToastController, public formBuilder: FormBuilder, private apiService: ApiService) {
    this.formGroup = this.formBuilder.group({
      fechaNacimiento: ['', [Validators.required]],
      nombrePadre: ['', [Validators.required, Validators.minLength(2)]],
      nombreMadre: ['', [Validators.required, Validators.minLength(2)]],
      seccion: ['', [Validators.required, Validators.minLength(2)]],
      grado: ['', [Validators.required, Validators.minLength(2)]],
    })
   }

  ngOnInit() {
  }

  async enviar(){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Por favor espera...',
      duration: 2000
    });
    await loading.present();  
    await loading.onDidDismiss(); 
    this.apiService.postAlumno(this.formGroup.value).subscribe(res => {
      console.log(res);
      this.mensaje("El alumno se ha guardado correctamente.", "success");
    }, error=>{
      this.mensaje("El alumno no se guardó correctamente.", "danger");
    })
  }

  async mensaje(texto, color) {
    const toast = await this.toastController.create({
      message: texto,
      duration: 2000,
      color: color
    });
    toast.present();
    setTimeout(() => {
      this.router.navigateByUrl('/home');
    }, 2000);
  }

}
